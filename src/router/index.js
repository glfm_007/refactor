import Vue from 'vue'
import VueRouter from 'vue-router'
import EventList from '../views/EventList.vue'
import EventCreate from '../views/EventCreate.vue';
import EventShow from '../views/EventShow.vue';
import UserView from '../views/UserView.vue';
import Auth from '../views/Auth/Auth.vue'
import Dashboard from '../views/Dashboard.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'dashboard',
    component: Dashboard,
    meta: { requiresAuth: true }
  },
  {
    path: '/post/:id',
    name: 'post-show',
    // component: Dashboard,
    meta: { requiresAuth: true }
  },
  {
    path: '/events',
    name: 'event-list',
    component: EventList,
    meta: { requiresAuth: true }
  },
  {
    path: '/event/:id',
    name: 'event-show',
    component: EventShow,
    props: true,
    meta: { requiresAuth: true }
  },
  {
    path: '/event/create',
    name: 'event-create',
    component: EventCreate,
    meta: { requiresAuth: true }
  },
  {
    path: '/event/:username',
    name: 'user',
    component: UserView,
    props: true,
    meta: { requiresAuth: true }
  },
  {
    path: '/auth',
    name: 'auth',
    component: Auth,
    // props: true
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const loggedIn = localStorage.getItem('user')

  if (to.matched.some(record => record.meta.requiresAuth) && !loggedIn) {
    next('/auth')
  }
  next()
})


export default router
