import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        user: { id: 'abc123', name: 'George Fermin' },
        categories: [
            'sustainability',
            'nature',
            'animal welfare',
            'housing',
            'education',
            'food',
            'community'
        ],
        todos: [
            { id: 1, text: '...', done: true },
            { id: 2, text: '...', done: false },
            { id: 3, text: '...', done: true },
            { id: 4, text: '...', done: false }
        ]
    },
    mutations: {
    },
    actions: {
    },
    getters: {

        getEventById: state => id => {
            return state.events.find(event => event.id === id)
        },
        catLength: state => {
            return state.categories.length
        },
        doneTodos: state => {
            return state.todos.filter(todo => todo.done)
        },
        activeTodosCount: state => {
            //Way #1 (need to pass (state, getters))
            // return state.todos.length - getters.doneTodos.length

            //Way #2 (recomended)
            return state.todos.filter(todo => !todo.done).length
        }
    },
    modules: {
    }
})
