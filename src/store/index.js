import Vue from 'vue'
import Vuex from 'vuex'
import * as user from '@/store/modules/user.js'
import Event from "./modules/event.js";
import Notification from "./modules/notification.js";
import Auth from "./modules/auth.js";
import Post from './modules/post.js';
import Comment from './modules/comment.js'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user,
    event: Event,
    notification: Notification,
    auth: Auth,
    post: Post,
    comment: Comment
  },
  state: {
    categories: [
      'sustainability',
      'nature',
      'animal welfare',
      'housing',
      'education',
      'food',
      'community'
    ],
  }
})
