import axios from 'axios'
import { api_routes } from '../../services/ApiService.js'

const state = {
    user: null
}

const actions = {
    register({ commit, dispatch }, credentials) {
        console.log(credentials);
        return axios.post(api_routes.user.register, credentials).then(
            ({ data }) => {
                commit('SET_USER', data)
            }
        )
    },

    login({ commit }, credentials) {
        return axios.post(api_routes.user.login, credentials).then(
            ({ data }) => {
                commit('SET_USER', data)
            }
        )
    },

    logout({ commit }) {
        commit('REMOVE_USER')
    }
}

const mutations = {
    SET_USER: (state, userData) => {
        state.user = userData
        localStorage.setItem('user', JSON.stringify(userData))
    },
    REMOVE_USER: () => {
        localStorage.removeItem('user')
        location.reload()
    }
}

const getters = {
    loggedIn(state) {
        console.log(state)
        return !!state.user
    }
}

export default {
    state,
    actions,
    getters,
    mutations
}