import axios from 'axios'
import { api_routes } from '../../services/ApiService'
import { authHeader } from '../../services/AuthService'
import CommentService from '../../services/CommentService';

const namespaced = true

const state = {
    comment: {},
    comments: []
}

const mutations = {
    ADD_COMMENT(state, comment) {
        state.comments.push(comment)
    },
    SET_COMMENTS(state, comments) {
        state.comments = comments
    },
    SET_COMMENT(state, comment) {
        state.comment = comment
    }
}

const actions = {
    createComment({ commit, dispatch }, comment) {
        return axios.post(api_routes.comments.post, comment, { headers: authHeader() }).then(() => {
            commit('ADD_POST', comment)
            const notification = {
                type: 'success',
                message: 'Your comment has successfully created!!'
            }
            dispatch('notification/add', notification, { root: true })
        })
            .catch(error => {
                const notification = {
                    type: 'error',
                    message: 'There was a problem creating your comment: ' + error.message
                }
                dispatch('notification/add', notification, { root: true })
                throw error
            });
    },

    fetchComments({ commit, dispatch }, id) {
        return CommentService.getComments(id).then(() => {
            console.log('comments response', response.data)
            commit('SET_COMMENTS', response.data)
        }).catch(error => {
            const notification = {
                type: 'error',
                message: 'Random message ' + error.message
            }
            dispatch('notification/add', notification, { root: true })
        })
        // return axios.get(api_routes.comments.all + id, { headers: authHeader() }).then(response => {
        //     console.log('comments response', response.data)
        //     commit('SET_COMMENTS', response.data)
        // })
        //     .catch(error => {
        //         const notification = {
        //             type: 'error',
        //             message: 'There was a problem fetching posts: ' + error.message
        //         }
        //         dispatch('notification/add', notification, { root: true })
        //     });
    },

    // fetchPost({ commit, dispatch }, id) {
    //     // var post = getters.getPostById(id)

    //     if (post) {
    //         commit('SET_POST', post)
    //     } else {
    //         return axios.get(api_routes.posts.all, id, { headers: authHeader() }).then(reponse => {
    //             commit('SET_EVENT', response.data)
    //         })
    //             .catch(error => {
    //                 const notification = {
    //                     type: 'error',
    //                     message: 'There was a problem fetching event: ' + error.message
    //                 }
    //                 dispatch('notification/add', notification, { root: true })
    //             });
    //     }
    // }

}

// const getters = {
//     getPostById: state => id => {
//         return state.posts.find(post => post.id === id)
//     },
// }

export default {
    state,
    mutations,
    actions,
    // getters,
    namespaced
}