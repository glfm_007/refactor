import axios from 'axios'
import { api_routes } from '../../services/ApiService'
import { authHeader } from '../../services/AuthService'
import PostService from '../../services/PostService'

const namespaced = true

const state = {
    post: {},
    posts: []
}

const mutations = {
    ADD_POST(state, post) {
        state.posts.push(post)
    },
    SET_POSTS(state, posts) {
        state.posts = posts
    },
    SET_POST(state, post) {
        state.post = post
    }
}

const actions = {
    createPost({ commit, dispatch }, post) {
        return axios.post(api_routes.posts.all, post, { headers: authHeader() }).then(() => {
            commit('ADD_POST', post)
            const notification = {
                type: 'success',
                message: 'Your post has successfully created!!'
            }
            dispatch('notification/add', notification, { root: true })
        })
            .catch(error => {
                const notification = {
                    type: 'error',
                    message: 'There was a problem creating your post: ' + error.message
                }
                dispatch('notification/add', notification, { root: true })
                throw error
            });
    },

    fetchPosts({ commit, dispatch }) {
        // return PostService.getPosts().then(() => {
        //     console.log('posts response', response.data)
        //     commit('SET_POSTS', response.data)
        // }).catch(error => {
        //     const notification = {
        //         type: 'error',
        //         message: 'Random message ' + error.message
        //     }
        //     dispatch('notification/add', notification, { root: true })
        // })
        console.log('entro', authHeader())
        return axios.get(api_routes.posts.all, { headers: authHeader() }).then(response => {
            console.log('fetchPost', response.data)
            commit('SET_POSTS', response.data)
        })
            .catch(error => {
                const notification = {
                    type: 'error',
                    message: 'There was a problem fetching posts: ' + error.message
                }
                dispatch('notification/add', notification, { root: true })
            });
    },

    fetchPost({ commit, dispatch }, id) {
        var post = getters.getPostById(id)

        if (post) {
            commit('SET_POST', post)
        } else {
            return axios.get(api_routes.posts.all, id, { headers: authHeader() }).then(response => {
                commit('SET_EVENT', response.data)
            })
                .catch(error => {
                    const notification = {
                        type: 'error',
                        message: 'There was a problem fetching event: ' + error.message
                    }
                    dispatch('notification/add', notification, { root: true })
                });
        }
    }

}

const getters = {
    getPostById: state => id => {
        return state.posts.find(post => post.id === id)
    },
}

export default {
    state,
    mutations,
    actions,
    getters,
    namespaced
}