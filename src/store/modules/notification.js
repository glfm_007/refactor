
const namespaced = true

const state = {
    notifications: []
}
const getters = {}
const actions = {
    add({ commit }, notification) {
        commit('PUSH', notification)
    },
    remove({ commit }, notificationToRemove) {
        commit('DELETE', notificationToRemove)
    }
}

let nextiId = 1
const mutations = {
    PUSH(state, notification) {
        state.notifications.push({
            ...notification,
            id: nextiId++
        })
    },
    DELETE(state, notificationToRemove) {
        state.notifications = state.notifications.filter(
            notification => notification.id !== notificationToRemove.id
        )
    }
}


export default {
    state,
    getters,
    actions,
    mutations,
    namespaced
}