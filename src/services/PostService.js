import axios from 'axios'

let user = JSON.parse(localStorage.getItem('user'));

const apiClient = axios.create({
    baseURL: 'http://localhost:8001/api',
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': user ? 'Bearer ' + user.access_token : {}

    }
})

export default {
    getPosts() {
        return apiClient.get('/post')
    },
    savePost(post) {
        return apiClient.post('/post', post)
    }
}