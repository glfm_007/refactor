import axios from 'axios'

let user = JSON.parse(localStorage.getItem('user'));

const apiClient = axios.create({
    baseURL: 'http://localhost:8001/api/',
    withCredentials: false,
    crossDomain: true,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': user ? 'Bearer ' + user.access_token : {}

    }
})

export default {
    getComments(id) {
        return apiClient.get('/comments/' + id)
    },
    postComments(comment) {
        return apiClient.post('/comments', comment)
    }
}