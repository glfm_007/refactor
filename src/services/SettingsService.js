let api;
// let api = 'http://18.207.221.15:8000/api/';

if (process.env.NODE_ENV == "production")
    api = "http://18.207.221.15:8000/api/";
else {
    api = "http://127.0.0.1:8001/api/auth/";
}

export const settings = {
    APP_NAME: "Graff",
    API_SERVER: api
};