import { settings } from './SettingsService.js';

const API_SERVER = settings.API_SERVER;

export const api_routes = {
    user: {
        login: API_SERVER + "login",
        register: API_SERVER + "register",
        refresh_token: API_SERVER + "refresh",
        logout: API_SERVER + "logout"
    },
    posts: {
        all: API_SERVER + "post",
    },
    comments: {
        all: API_SERVER + "comments/",
    }
}
